package com.example.demo.controller;

import com.example.demo.dto.UserCreateDto;
import com.example.demo.dto.UserDto;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/api/users")
public class UserController {
    @Autowired private UserService userService;

    @GetMapping("/")
    List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("/")
    UserDto addNewUser(@RequestBody UserCreateDto userCreateDto) {
        return userService.createNewUser(userCreateDto);
    }

    @GetMapping("/name/{firstName}")
    List<UserDto> getAllUserByFirstName(@PathVariable String firstName) {
        return userService.getAllUsersByFirstName(firstName);
    }

    @GetMapping("/{userId}")
    UserDto getUserById(@PathVariable long userId) {
        return userService.getUserById(userId);
    }
}

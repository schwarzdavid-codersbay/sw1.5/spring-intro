package com.example.demo.controller;

import com.example.demo.dto.UserDto;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;

@RestController()
@RequestMapping("/api/test")
public class TestController {
    private HashSet<UserDto> users = new HashSet<>();

    @GetMapping("/")
    HashSet<UserDto> getAllUsers() {
        return users;
    }

    @PostMapping("/")
    UserDto addNewUser(@RequestBody UserDto user) {
        users.add(user);
        return user;
    }

    /*@GetMapping("/{index}")
    String getUserByIndex(@PathVariable int index) {
        return users.get(index);
    }

    @PutMapping("/{index}")
    String editUserByIndex(@PathVariable int index, @RequestBody String newName) {
        users.set(index, newName);
        return users.get(index);
    }

    @DeleteMapping("/{index}")
    ArrayList<String> deleteUserByIndex(@PathVariable int index) {
        users.remove(index);
        return users;
    }*/

}

package com.example.demo.service;

import com.example.demo.dto.UserCreateDto;
import com.example.demo.dto.UserDto;
import com.example.demo.model.UserModel;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired private UserRepository userRepository;

    public List<UserDto> getAllUsersByFirstName(String firstName) {
        Iterable<UserModel> userModels = userRepository.findAllByFirstName(firstName);
        return convertUserModelListToDtos(userModels);
    }

    public UserDto getUserById(long userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if(userModelOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        UserModel userModel = userModelOptional.get();
        return convertUserModelToDto(userModel);
    }

    public List<UserDto> getAllUsers() {
        Iterable<UserModel> userModels = userRepository.findAll();
        return convertUserModelListToDtos(userModels);
    }

    public UserDto createNewUser(UserCreateDto userCreateDto) {
        UserModel userModel = new UserModel()
                .setLastName(userCreateDto.getLastName())
                .setFirstName(userCreateDto.getFirstName());

        userRepository.save(userModel);

        return convertUserModelToDto(userModel);
    }

    private static UserDto convertUserModelToDto(UserModel userModel) {
        return new UserDto()
                .setUserId(userModel.getUserId())
                .setFirstName(userModel.getFirstName())
                .setLastName(userModel.getLastName());
    }

    private static List<UserDto> convertUserModelListToDtos(Iterable<UserModel> userModels) {
        List<UserDto> userDtos = new ArrayList<>();

        for(UserModel userModel : userModels) {
            UserDto userDto = convertUserModelToDto(userModel);
            userDtos.add(userDto);
        }

        return userDtos;
    }
}

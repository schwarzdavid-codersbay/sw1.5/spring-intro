package com.example.demo.model;

import jakarta.persistence.*;

@Entity
@Table
public class UserModel {
    @Id
    @GeneratedValue
    @Column(nullable = false)
    private long userId;

    @Column
    private String firstName;

    @Column
    private String lastName;

    public UserModel(long userId, String firstName, String lastName) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserModel() {
    }

    public long getUserId() {
        return userId;
    }

    public UserModel setUserId(long userId) {
        this.userId = userId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public UserModel setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserModel setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
}

package com.example.demo.dto;

public class UserCreateDto {
    String firstName;
    String lastName;

    public UserCreateDto(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserCreateDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public UserCreateDto setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserCreateDto setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
}
